from db import db
from flask import jsonify

class delete:

    @staticmethod
    def deleteItem(tableName, args):
        argErrors = {}
        if tableName in db.schema:
            columns = {key: db.schema[tableName][key] for key in db.schema[tableName] if db.schema[tableName][key][2] == "PRI"}
            # First, we'll verify that each arg is a primary key
            for argKey in args.keys():
                if argKey not in columns:
                    argErrors[argKey] = f"Column '{argKey}' not a primary key in table '{tableName}'"
            sqlStr1 = f"DELETE FROM {tableName} WHERE "
            sqlStr2 = f"SELECT COUNT(*) FROM {tableName} WHERE "
            filter = []
            # Now, for each of the primary keys, add them in to the arg list for the command
            # If they aren't all present, that's an error
            for column in list(columns.keys()):
                if column in args:
                    if not db.isValidDataType(columns[column][0], args[column]):
                        argErrors[column] = f"Invalid data '{args[column]}' for column {column}"
                    sqlStr1 += f"{column}=%s AND "
                    sqlStr2 += f"{column}=%s AND "
                    filter.append(args[column])
                else:
                    argErrors[column] = f"Missing primary key '{column}'"
            sqlStr1 = sqlStr1[:-4]
            sqlStr2 = sqlStr2[:-4]
            matchCount = db.executeCommand(sqlStr2, filter)
            if len(argErrors.keys()) == 0 and matchCount[0][0] < 1:
                argErrors["NoEntry"] = f"Could not find matching entry to be deleted"
            if len(argErrors.keys()) == 0:
                db.executeCommand(sqlStr1, filter)
                db.commit()
        else:
            argErrors[tableName] = f"Table '{tableName}' not found"
        if len(argErrors.keys()) > 0:
            return jsonify(
                {
                    "status": "fail",
                    "data": argErrors
                }
            )
        else:
            return jsonify(
                {
                    "status": "success",
                    "data": {}
                }
            )