from db import db
from flask import jsonify

class patch:

    @staticmethod
    def patchItem(tableName, args):
        argErrors = {}
        if tableName in db.schema:
            columns = db.schema[tableName]
            # First, we'll verify that each arg is a column
            for argKey in args.keys():
                if argKey not in columns:
                    argErrors[argKey] = f"Column '{argKey}' not found in table '{tableName}'"
            sqlStr1 = f"UPDATE {tableName} SET "
            sqlStr2 = "WHERE "
            filter1 = []
            filter2 = []
            numChanges = 0
            # Now, we can go column by column of the schema, and add it to our patch statement
            # This way, we can throw errors if a required column value has no args, or if
            # a non-null foreign key doesn't match
            for column in list(columns.keys()):
                if column in args and args[column] != '':
                    foreignKey = [key for key in db.foreignKeys if key[0].lower() == tableName.lower() and key[1].lower() == column.lower()]
                    if len(foreignKey) > 0: # If this column is a foreign key, and was assigned a value, check to make sure it exists
                        sqlKeyCheckStr = f"SELECT COUNT(*) FROM {foreignKey[0][2]} WHERE {foreignKey[0][3]}=%s"
                        sqlKeyCheckVal = [args[column]]
                        numMatches = db.executeCommand(sqlKeyCheckStr, sqlKeyCheckVal)
                        if numMatches[0][0] < 1:
                            argErrors[column] = f"Can't set {column} to value '{args[column]}' as there is no corresponding key"
                    if not db.isValidDataType(columns[column][0], args[column]):
                        argErrors[column] = f"Invalid data '{args[column]}' for column {column}"
                    if columns[column][2] == "PRI": # If this is a primary key
                        sqlKeyCheckStr = f"SELECT COUNT(*) FROM {tableName} WHERE {column}=%s"
                        sqlKeyCheckVal = [args[column]]
                        numMatches = db.executeCommand(sqlKeyCheckStr, sqlKeyCheckVal)
                        if numMatches[0][0] < 1:
                            argErrors[column] = f"Can't find record with key {args[column]}"
                        sqlStr2 += f"{column}=%s AND "
                        filter2.append(args[column])
                    else:
                        numChanges += 1
                        sqlStr1 += f"{column}=%s, "
                        filter1.append(args[column])
                else:
                    if columns[column][2] == "PRI": # If this is a primary key
                        argErrors[column] = f"Must designate record to be edited by primary key '{column}'"
            sqlStr = sqlStr1[:-2] + " " + sqlStr2[:-4]
            if numChanges < 1:
                argErrors["NoChanges"] = f"Must change the value of a column to patch"
            if len(argErrors.keys()) == 0:
                db.executeCommand(sqlStr, filter1 + filter2)
                db.commit()
        else:
            argErrors[tableName] = f"Table '{tableName}' not found"
        if len(argErrors.keys()) > 0:
            return jsonify(
                {
                    "status": "fail",
                    "data": argErrors
                }
            )
        else:
            return jsonify(
                {
                    "status": "success",
                    "data": {}
                }
            )