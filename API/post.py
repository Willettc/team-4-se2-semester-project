from db import db
from flask import jsonify

class post:

    @staticmethod
    def postItem(tableName, args):
        argErrors = {}
        if tableName in db.schema:
            columns = db.schema[tableName]
            # First, we'll verify that each arg is a column
            for argKey in args.keys():
                if argKey not in columns:
                    argErrors[argKey] = f"Column '{argKey}' not found in table '{tableName}'"
            sqlStr1 = f"INSERT INTO {tableName} ("
            sqlStr2 = "VALUES ("
            retData = {}
            filter = []
            # Now, we can go column by column of the schema, and add it to our insert statement
            # This way, we can throw errors if a required column value has no args, or if
            # a non-null foreign key doesn't match
            for column in list(columns.keys()):
                sqlStr1 += column + ", "
                sqlStr2 += "%s, "
                if column in args and args[column] != '':
                    foreignKey = [key for key in db.foreignKeys if key[0].lower() == tableName.lower() and key[1].lower() == column.lower()]
                    if len(foreignKey) > 0: # If this column is a foreign key, and was assigned a value, check to make sure it exists
                        sqlKeyCheckStr = f"SELECT COUNT(*) FROM {foreignKey[0][2]} WHERE {foreignKey[0][3]}=%s"
                        sqlKeyCheckVal = [args[column]]
                        numMatches = db.executeCommand(sqlKeyCheckStr, sqlKeyCheckVal)
                        if numMatches[0][0] < 1:
                            argErrors[column] = f"Can't add {column} with value '{args[column]}' as there is no corresponding key"
                    if not db.isValidDataType(columns[column][0], args[column]):
                        argErrors[column] = f"Invalid data '{args[column]}' for column {column}"
                    filter.append(args[column])
                    retData[column] = args[column]
                else:
                    if columns[column][1] == "NO": # If this column isn't nullable
                        argErrors[column] = f"Column {column} must be assigned a value"
                    filter.append(None)
                    retData[column] = None
            sqlStr = sqlStr1[:-2] + ") " + sqlStr2[:-2] + ")"
            if len(argErrors.keys()) == 0:
                db.executeCommand(sqlStr, filter)
                db.commit()
        else:
            argErrors[tableName] = f"Table '{tableName}' not found"
        if len(argErrors.keys()) > 0:
            return jsonify(
                {
                    "status": "fail",
                    "data": argErrors
                }
            )
        else:
            return jsonify(
                {
                    "status": "success",
                    "data": retData
                }
            )