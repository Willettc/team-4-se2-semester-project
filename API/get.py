from db import db
from flask import jsonify

class get:

    @staticmethod
    def showTableList():
        return jsonify(
                {
                    "status": "success",
                    "data": {
                        "tables": list(db.schema.keys())
                    }
                }
            )

    @staticmethod
    def showTableInfo(tableName):
        retString = ""
        if tableName in db.schema:
            return jsonify(
                {
                    "status": "success",
                    "data": db.schema[tableName]
                }
            )
        else:
            return jsonify(
                {
                    "status": "fail",
                    "data": {
                        "table": f"Table '{tableName}' not found"
                    }
                }
            )


    @staticmethod
    def getItems(tableName, args):
        argErrors = {}
        data = []
        if tableName in db.schema:
            columns = db.schema[tableName]
            sqlStr = f"SELECT * FROM {tableName} WHERE "
            filter = []
            for argKey in args.keys():
                if argKey in columns:
                    columnType = columns[argKey][0]
                    if args[argKey] == '': # Ignore empty fields for now
                        pass
                    elif db.isValidDataType(columnType, args[argKey]):
                        sqlStr += f"{argKey}=%s AND "
                        filter.append(args[argKey])
                    else:
                        argErrors[argKey] = f"Invalid data entered for column '{argKey}'"
                else:
                    argErrors[argKey] = f"Column '{argKey}' not found in table '{tableName}'"
            sqlStr = sqlStr[:-5]
            dataArr = db.executeCommand(sqlStr, filter)
            for result in dataArr:
                resultDict = {}
                for i in range(len(list(columns.keys()))):
                    resultDict[list(columns.keys())[i]] = str(result[i])
                data.append(resultDict)
        else:
            argErrors[tableName] = f"Table '{tableName}' not found"
        if len(argErrors.keys()) > 0:
            return jsonify(
                {
                    "status": "fail",
                    "data": argErrors
                }
            )
        else:
            return jsonify(
                {
                    "status": "success",
                    "data": data
                }
            )