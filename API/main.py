import flask
import mysql.connector
from db import db
from get import get
from post import post
from patch import patch
from delete import delete
from shopPage import shopPage
from flask import request, jsonify


app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/api/raw/<string:tableName>/get', methods=['GET'])
def createGetForm(tableName):
    if not db.verifyConnection():
        return jsonify(
            {
                "status": "fail",
                "data": {
                    "database": "Failed to properly connect to database"
                    }
            }
        )
    retStr = ""
    if tableName in db.schema:
        retStr += f'<h1>{tableName}:</h1>'
        retStr += f'<form action="/{tableName}/" method="get">\n'
        for column in list(db.schema[tableName].keys()):
            retStr += f'<label for="{column}">{column}:</label>\n'
            retStr += f'<input type="text" id="{column}" name="{column}"><br><br>\n'
        retStr += '<button type="submit">Submit</button>\n'
        retStr += '</form>'
    else:
        retStr += f"<h1>No such table '{tableName}'</h1>"
    return retStr

@app.route('/api/raw/<string:tableName>/', methods=['GET'])
def getData(tableName):
    if not db.verifyConnection():
        return jsonify(
            {
                "status": "fail",
                "data": {
                    "database": "Failed to properly connect to database"
                    }
            }
        )
    if len(request.args) < 1:
        return get.showTableInfo(tableName)
    else:
        return get.getItems(tableName, request.args)

@app.route('/api/raw/', methods=['GET'])
def getSchema():
    if not db.verifyConnection():
        return jsonify(
            {
                "status": "fail",
                "data": {
                    "database": "Failed to properly connect to database"
                    }
            }
        )
    return get.showTableList()

@app.route('/api/raw/<string:tableName>/post', methods=['GET'])
def createPostForm(tableName):
    if not db.verifyConnection():
        return jsonify(
            {
                "status": "fail",
                "data": {
                    "database": "Failed to properly connect to database"
                    }
            }
        )
    retStr = ""
    if tableName in db.schema:
        retStr += f'<h1>{tableName}:</h1>'
        retStr += f'<form action="/{tableName}/" method="post">\n'
        for column in list(db.schema[tableName].keys()):
            retStr += f'<label for="{column}">{column}:</label>\n'
            retStr += f'<input type="text" id="{column}" name="{column}"><br><br>\n'
        retStr += '<button type="submit">Submit</button>\n'
        retStr += '</form>'
    else:
        retStr += f"<h1>No such table '{tableName}'</h1>"
    return retStr

@app.route('/api/raw/<string:tableName>/', methods=['POST'])
def addData(tableName):
    if not db.verifyConnection():
        return jsonify(
            {
                "status": "fail",
                "data": {
                    "database": "Failed to properly connect to database"
                    }
            }
        )
    return post.postItem(tableName, request.form)

@app.route('/api/raw/<string:tableName>/', methods=['PATCH'])
def editData(tableName):
    if not db.verifyConnection():
        return jsonify(
            {
                "status": "fail",
                "data": {
                    "database": "Failed to properly connect to database"
                    }
            }
        )
    return patch.patchItem(tableName, request.form)

@app.route('/api/raw/<string:tableName>/', methods=['DELETE'])
def removeData(tableName):
    if not db.verifyConnection():
        return jsonify(
            {
                "status": "fail",
                "data": {
                    "database": "Failed to properly connect to database"
                    }
            }
        )
    return delete.deleteItem(tableName, request.form)

@app.route('/api/shop/showbikes', methods=['GET'])
def getPage():
    if not db.verifyConnection():
        return jsonify(
            {
                "status": "fail",
                "data": {
                    "database": "Failed to properly connect to database"
                    }
            }
        )
    return shopPage.getPage(request.args)


if(__name__ == "__main__"):
    db.init()
    app.run()
