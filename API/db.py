import mysql.connector
import datetime
import string



class db:
    db = None
    cursor = None
    schema = None
    foreignKeys = None
    initialized = False

    @staticmethod
    def init():
        try:
            db.db = mysql.connector.connect(
                host="se2bikeshopdatabase.cj0mftxuyuoo.us-east-1.rds.amazonaws.com",
                user="admin",
                password="axTXNLjs861AJmcWxwsr",
                database="BIKE_SHOP"
            )
            db.schema = {}

            db.cursor = db.db.cursor()
            db.cursor.execute("SHOW TABLES")
            tableNames = db.cursor.fetchall()
            for tableName in tableNames:
                db.cursor.execute(f"DESCRIBE {tableName[0].decode('UTF-8')}")
                tableInfo = db.cursor.fetchall()
                tableDict = {}
                for column in tableInfo:
                    columnList = list(column[1:])
                    for i in range(len(columnList)):
                        try:
                            columnList[i] = columnList[i].decode()
                        except:
                            pass
                    tableDict[column[0]] = columnList
                db.schema[tableName[0].decode('UTF-8')] = tableDict

            db.cursor.execute("SELECT TABLE_NAME, COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = 'BIKE_SHOP'")
            db.foreignKeys = db.cursor.fetchall()
            db.initialized = True
        except:
            db.initialized = False

    @staticmethod
    def verifyConnection():
        if not db.initialized:
            db.init()
        return db.initialized

    @staticmethod
    def executeCommand(command, args):
        db.cursor.execute(command, tuple(args))
        return db.cursor.fetchall()

    @staticmethod
    def commit():
        db.db.commit()

    @staticmethod
    def isValidDataType(columnType, data):
        if(columnType.startswith("varchar")):
            strLength = columnType[8:-1]
            return int(strLength) > len(data)
        elif(columnType.startswith("double")):
            try:
                float(data)
                return True
            except ValueError:
                return False
        elif(columnType.startswith("decimal")):
            brokenStr = columnType.split(',')
            maxLen = brokenStr[0][8:]
            minLen = brokenStr[1][:-1]
            return data.isdigit() and int(maxLen) >= len(data) and int(minLen) <= len(data)
        elif(columnType.startswith("datetime")):
            # Validation of time format taken from https://stackoverflow.com/questions/16870663/how-do-i-validate-a-date-string-format-in-python/16870699
            try:
                datetime.datetime.strptime(data, '%Y-%m-%d')
                return True
            except ValueError:
                return False
        return False
