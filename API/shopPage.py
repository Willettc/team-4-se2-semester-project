from db import db
from flask import jsonify

class shopPage:

    @staticmethod
    def getPage(args):
        argErrors = {}
        data = []
        argVals = []
        whereUsed = False
        pageNum = 1
        perPage = 5
        bikeType = None
        color = None
        minPrice = None
        maxPrice = None
        for argKey in args.keys():
            if argKey == "page":
                try:
                    pageNum = int(args[argKey])
                    if pageNum < 1:
                        argErrors["page"] = "Invalid value for page number"
                except ValueError:
                    argErrors["page"] = "Invalid value for page number"
            elif argKey == "perpage":
                try:
                    perPage = int(args[argKey])
                    if perPage < 1:
                        argErrors["perPage"] = "Invalid value for num per page"
                except ValueError:
                    argErrors["perPage"] = "Invalid value for num per page"
            elif argKey == "biketype":
                bikeType = args[argKey].split(',')
            elif argKey == "color":
                color = args[argKey].replace("-"," ").split(',')
            elif argKey == "minprice":
                try:
                    minPrice = float(args[argKey])
                    if minPrice < 0:
                        argErrors["minprice"] = "Invalid value for minprice"
                except ValueError:
                    argErrors["minprice"] = "Invalid value for minprice"
            elif argKey == "maxprice":
                try:
                    maxPrice = float(args[argKey])
                    if maxPrice < 0:
                        argErrors["maxprice"] = "Invalid value for maxprice"
                except ValueError:
                    argErrors["maxprice"] = "Invalid value for maxprice"
            else:
                argErrors[argKey] = "Invalid key"
        sqlStr = f"SELECT BICYCLE.SERIALNUMBER, BICYCLE.FRAMESIZE, BICYCLE.MODELTYPE, BICYCLE.SALEPRICE, PAINT.COLORNAME "
        sqlStr +=f"FROM BICYCLE LEFT JOIN PAINT USING (PAINTID) "

        if bikeType is not None:
            if not whereUsed:
                sqlStr += "WHERE ("
                whereUsed = True
            else:
                sqlStr += "AND ("
            for bike in bikeType:
                sqlStr += f"BICYCLE.MODELTYPE = %s OR "
            sqlStr = sqlStr[:-4] + ") "
            argVals = argVals + bikeType

        if color is not None:
            if not whereUsed:
                sqlStr += "WHERE ("
                whereUsed = True
            else:
                sqlStr += "AND ("
            for obj in color:
                sqlStr += f"PAINT.COLORNAME = %s OR "
            sqlStr = sqlStr[:-4] + ") "
            argVals = argVals + color

        if minPrice is not None:
            if not whereUsed:
                sqlStr += "WHERE "
                whereUsed = True
            else:
                sqlStr += "AND "
            sqlStr += f"BICYCLE.SALEPRICE >= %s "
            argVals.append(minPrice)

        if maxPrice is not None:
            if not whereUsed:
                sqlStr += "WHERE "
                whereUsed = True
            else:
                sqlStr += "AND "
            sqlStr += f"BICYCLE.SALEPRICE <= %s "
            argVals.append(maxPrice)

        sqlStr += f"ORDER BY BICYCLE.SERIALNUMBER LIMIT %s OFFSET %s"
        argVals.append(perPage)
        argVals.append(perPage * (pageNum - 1))

        if len(argErrors.keys()) > 0:
            return jsonify(
                {
                    "status": "fail",
                    "data": argErrors
                }
            )
        else:
            dataArr = db.executeCommand(sqlStr, argVals)
            data = []
            for result in dataArr:
                data.append(
                    {
                        "bikeid": str(result[0]),
                        "framesize": str(result[1]),
                        "modeltype": str(result[2]),
                        "salesprice": str(result[3])[:-2],
                        "color": str(result[4])
                    }
                )
            return jsonify(
                {
                    "status": "success",
                    "data": data
                }
            )