"""

Name: Simon Bike Shop Testing Script
Filename: SimonBikeShopTest.py
Author: Simon Cichonski
Last Revision: Thursday, April 1, 2021

"""

# Please take the time to install webdriver-manager using pip through the terminal

# Keyboard shortcut: ALT + F12
# Enter: pip install webdriver-manager

# Now you can run my script! :)
# CTRL + SHIFT + F10

# Note: I print testing successes/failures through the console

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import time
from selenium.webdriver.common.keys import Keys
from datetime import datetime
import sys # for file i/o


# Download driver. Begin browser, and navigate to http://107.22.138.83:8000/
driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("http://107.22.138.83:8000/")

# This counter is an ID for each successful testing point
counter = 0

# Maximize window
driver.maximize_window()

"""
Function go_to_page will force driver to wait before locating the element
that navigates to the next page using the element's xpath
Parameters:
    STRING xpath
    INT wait_time
"""


def go_to_page(xpath, wait_time):
    try:
        global counter
        counter += 1
        page = driver.find_element_by_xpath(xpath)
        driver.execute_script("arguments[0].click();", page)
        time.sleep(wait_time)
        print(str(counter) + ") " + str(datetime.now().strftime("%H:%M:%S") + " Go to Page: " + xpath + " -- success"),
              file=open("team4frontendlog.txt", "a"))
    except:
        print(str(datetime.now().strftime("%H:%M:%S") + " Go to Page: " + xpath + " -- failure"))

"""
Function form_fill selects the element form and fills it with a string
Parameters:
    STRING xpath
    STRING keys
    INT wait_time
"""


def form_fill(xpath, keys, wait_time):
    try:
        global counter
        counter += 1
        driver.find_element_by_xpath(xpath).send_keys(keys)
        time.sleep(wait_time)
        print(str(counter) + ") " + str(datetime.now().strftime("%H:%M:%S") + " Form Fill: " + xpath + " -- success"),
              file=open("team4frontendlog.txt", "a"))
    except:
        print(str(datetime.now().strftime("%H:%M:%S") + " Form Fill: " + xpath + " -- failure"))

"""
!
    Begin -NORMAL FLOW-
!
"""
# Wait 3 seconds for page load, then select "About" navbar item
time.sleep(3)
go_to_page("/html/body/nav/div/div/ul/li[1]/a", 5)

# Wait 2 seconds, then select "Projects" navbar item
go_to_page("/html/body/nav/div/div/ul/li[2]/a", 2)

# Wait 2 seconds, then select "Contact" navbar item
go_to_page("/html/body/nav/div/div/ul/li[3]/a", 2)
driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)  # Scroll up

# Wait 2 seconds before proceeding to the "Signup" page
go_to_page("/html/body/nav/div/div/ul/li[6]/a", 2)

# Fill in form data
form_fill('/html/body/section/form/p[1]/input', 'test_user', 1)  # username: test_user
form_fill('/html/body/section/form/p[2]/input', 'test', 1)  # first_name: test
form_fill('/html/body/section/form/p[3]/input', 'user', 1)  # last_name: user
form_fill('/html/body/section/form/p[4]/input', 'test_user@test.com', 1)  # email: test_user@test.com
form_fill('/html/body/section/form/p[5]/input', 'Passw0rd!', 1)  # password: Passw0rd!
form_fill('/html/body/section/form/p[7]/input', 'Passw0rd!', 1)  # password_confirm: Passw0rd!

# Wait 2 seconds before signing up
go_to_page("/html/body/section/form/button", 2)

# Sign in screen
driver.get("http://107.22.138.83:8000/")
go_to_page("/html/body/nav/div/div/ul/li[5]/a", 2)

# Log in as the test user
form_fill('/html/body/form/table/tbody/tr[1]/td[2]/input', 'test_user', 1)
form_fill('/html/body/form/table/tbody/tr[2]/td[2]/input', 'Passw0rd!', 1)
go_to_page("/html/body/form/input[2]", 1)

# Wait 2 seconds to proceed to view the Shopping Cart
go_to_page("/html/body/nav/div/div/ul/li[7]/a/img", 2)

# Clear the shopping cart after 2 seconds
go_to_page("/html/body/div/div/div[1]/button", 2)

# Return home by clicking the "Team 4" on the navbar after 2 seconds
go_to_page("/html/body/nav/div/a", 2)

# Proceed to "Products"
go_to_page("/html/body/nav/div/div/ul/li[4]/a", 2)

# Add the first road bike
go_to_page("/html/body/div/div/div/div/div/div/div[2]/div[2]/table/tbody/tr[1]/td[6]/button", 2)

# Turns out we want 3 bikes, not 1
go_to_page("/html/body/div/div/div[2]/table/tbody/tr/td[4]/button", 2)
go_to_page("/html/body/div/div/div[2]/table/tbody/tr/td[4]/button", 2)

# Return to "Products"
go_to_page("/html/body/nav/div/div/ul/li[4]/a", 2)

# Let's filter by Tour, Mountain, and Road bikes
go_to_page("/html/body/div/div/div/div/div/div/div[1]/form/div/label[2]/input", 1)
go_to_page("/html/body/div/div/div/div/div/div/div[1]/form/div/label[4]/input" ,1)
go_to_page("/html/body/div/div/div/div/div/div/div[1]/form/div/label[5]/input" ,1)

# Let's filter by Neon Blue, Hazard Flame, Grey Granite, and Black Hole
go_to_page("/html/body/div/div/div/div/div/div/div[1]/form/fieldset/div[1]/table/tbody/tr[1]/td[1]/label/input", 1)
go_to_page("/html/body/div/div/div/div/div/div/div[1]/form/fieldset/div[1]/table/tbody/tr[3]/td[3]/label/input", 1)
go_to_page("/html/body/div/div/div/div/div/div/div[1]/form/fieldset/div[1]/table/tbody/tr[4]/td[2]/label/input", 1)
go_to_page("/html/body/div/div/div/div/div/div/div[1]/form/fieldset/div[1]/table/tbody/tr[5]/td[3]/label/input", 1)

# Let's filter price price. Min - 1000.00 and Max - 2000.00
form_fill('/html/body/div/div/div/div/div/div/div[1]/form/fieldset/div[4]/div[1]/input', '1000.00', 1)
form_fill('/html/body/div/div/div/div/div/div/div[1]/form/fieldset/div[4]/div[2]/input', '2000.00', 1)

# Click the "Filter Results" button
go_to_page("/html/body/div/div/div/div/div/div/div[1]/form/input", 2)

# Let's add the third one
go_to_page("/html/body/div/div/div/div/div/div/div[2]/div[2]/table/tbody/tr[3]/td[6]/button", 2)

# Go to Shopping Cart
go_to_page("/html/body/nav/div/div/ul/li[7]/a/img", 2)

# Turns out I'd prefer to have the Mountain Bike to myself, not a Road Bike
go_to_page("/html/body/div/div/div[2]/table/tbody/tr[1]/td[5]/button", 2)

# Oh, I just got stimulated. I'll gift my friends two luxurious mountain bikes
go_to_page("/html/body/div/div/div[2]/table/tbody/tr[1]/td[7]/button", 2)
go_to_page("/html/body/div/div/div[2]/table/tbody/tr/td[4]/button", 2)
go_to_page("/html/body/div/div/div[2]/table/tbody/tr/td[4]/button", 2)

# Sign out, I'll pay sometime else
go_to_page("/html/body/nav/div/div/ul/li[6]/a", 2)

# Sign in screen
driver.get("http://107.22.138.83:8000/")
go_to_page("/html/body/nav/div/div/ul/li[5]/a", 2)

# Log in as a superuser
form_fill('/html/body/form/table/tbody/tr[1]/td[2]/input', 'team4', 1)
form_fill('/html/body/form/table/tbody/tr[2]/td[2]/input', 'Passw0rd!', 1)
go_to_page("/html/body/form/input[2]", 1)

# Go to admin screen
driver.get("http://107.22.138.83:8000/admin/")

# To to Users
go_to_page('/html/body/div/div[2]/div/div[1]/div[1]/div/table/tbody/tr[2]/th/a', 1)

# Select the user we just created
go_to_page('/html/body/div/div[3]/div/div[1]/div/div/div[1]/form/div[2]/table/tbody/tr[8]/td[1]/input', 2)

# Select action
dropdown = driver.find_element_by_xpath(("/html/body/div/div[3]/div/div[1]/div/div/div[1]/form/div[1]/label/select"))
dropdown.send_keys("Delete selected users")

# Delete user
go_to_page("/html/body/div/div[3]/div/div[1]/div/div/div[1]/form/div[1]/button", 1)

# Yes, I'm sure
go_to_page("/html/body/div/div[3]/div/div[1]/form/div/input[4]", 1)

"""
    Begin - EXCEPTION FLOW -
"""

# Return home
driver.get("http://107.22.138.83:8000/")

# Log out
go_to_page("/html/body/nav/div/div/ul/li[6]/a", 1)

# Attempt sign-in as non-existent user and head to the "Sign Up" page
driver.get("http://107.22.138.83:8000/")
go_to_page("/html/body/nav/div/div/ul/li[5]/a", 1)
form_fill('/html/body/form/table/tbody/tr[1]/td[2]/input', 'nonexistentuser', 2)  # Username: nonexistentuser
form_fill('/html/body/form/table/tbody/tr[2]/td[2]/input', 'Passw0rd!', 1)  # Password: Passw0rd!
go_to_page("/html/body/form/input[2]", 1)

time.sleep(3)

# Go to signup page
go_to_page("/html/body/nav/div/div/ul/li[6]/a", 1)

# Fill in form data w/ wrong password confirm
form_fill('/html/body/section/form/p[1]/input', 'test_user', 1)  # username: test_user
form_fill('/html/body/section/form/p[2]/input', 'test', 1)  # first_name: test
form_fill('/html/body/section/form/p[3]/input', 'user', 1)  # last_name: user
form_fill('/html/body/section/form/p[4]/input', 'test_user@test.com', 1)  # email: test_user@test.com
form_fill('/html/body/section/form/p[5]/input', 'Passw0rd!', 1)  # password: Passw0rd!

# This line is incorrect
form_fill('/html/body/section/form/p[7]/input', 'Passw0rd!!', 1)  # password_confirm: Passw0rd!!

go_to_page("/html/body/nav/div/div/ul/li[6]/a", 1)

# Let's leave username blank
form_fill('/html/body/section/form/p[1]/input', '', 1)  # username: NULL
form_fill('/html/body/section/form/p[2]/input', 'test', 1)  # first_name: test
form_fill('/html/body/section/form/p[3]/input', 'user', 1)  # last_name: user
form_fill('/html/body/section/form/p[4]/input', 'test_user@test.com', 1)  # email: test_user@test.com
form_fill('/html/body/section/form/p[5]/input', 'Passw0rd!', 1)  # password: Passw0rd!
form_fill('/html/body/section/form/p[7]/input', 'Passw0rd!', 1)  # password_confirm: Passw0rd!

go_to_page("/html/body/section/form/button", 1)