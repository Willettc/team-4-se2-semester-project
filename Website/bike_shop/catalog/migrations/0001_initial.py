# Generated by Django 3.1.7 on 2021-03-15 23:25

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bike',
            fields=[
                ('bikeId', models.AutoField(primary_key=True, serialize=False)),
                ('color', models.CharField(max_length=200)),
                ('frameSize', models.DecimalField(decimal_places=3, max_digits=10)),
                ('modelType', models.CharField(max_length=100)),
                ('salesPrice', models.DecimalField(decimal_places=2, max_digits=50)),
            ],
        ),
    ]
