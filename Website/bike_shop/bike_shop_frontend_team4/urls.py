from django.urls import path, re_path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from . import views as core_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
        path('admin/', admin.site.urls),
        path('', views.index, name='index'),
        path('<int:bike_id>/details', views.bike_details,name='details'),
        #87295 is the value for Current Manager
        path('<int:employee_id>/manager', views.manager,name='manager'),
        #Any other value is NOT a manager
        path('<int:employee_id>/employee', views.employee,name='employee'),   

        path('css/styles.css', views.css, name='css'),
        path('js/scripts.js', views.js, name='js'),
        path('assets/img/ipad.png',views.ipad, name='ipad.png'),
        path('assets/img/bgmasthead.jpg',views.bgmasthead, name='bgmasthead.jpg'),
        path('assets/img/demoimageone.jpg',views.demoimageone, name='demoimageone.jpg'),
        path('assets/img/demoimagetwo.jpg', views.demoimagetwo, name='demoimagetwo.jpg'),
        url(r'^signup/$', core_views.signup, name='signup'),
        path('cart', views.shopping_cart, name='shopping_cart'),
        url(r'^logout/$', views.logout_view, name='logout'),
        url(r'^bikes/filter/?.*', views.bikes_get, name='bike_get'),
        url('bikes', views.bikes_view, name='bikes'),
        path('bike/<int:bikeid>', views.bike_details, name='bike_details'),
        url('shoppingCart', views.shopping_cart, name='cart'),
    ]

# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += [
        path('accounts/', include('django.contrib.auth.urls')),
        ]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
