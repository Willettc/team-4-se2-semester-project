import json

import requests
from django.http import HttpResponse, QueryDict
from django.shortcuts import render
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from .forms import SignUpForm
from django.http import Http404



def index(request):
    return render(request, 'index.html')


def bike_details(request, bike_id):
    return render(request, 'template.html')


def manager(request, employee_id):
    return render(request, 'index.html')


def employee(request, employee_id):
    return render(request, 'index.html')


def login(request):
    return render(request,'index.html')


def css(request):
    return render(request,'css/styles.css')


def js(request):
    return render(request,'js/scripts.js')


def ipad(request):
    return render(request,'assets/img/ipad.png', content_type='image/png')


def bgmasthead(request):
    return render(request,'assets/img/bgmasthead.jpg', content_type='image/jpeg')


def demoimageone(request):
    return render(request,'assets/img/demoimageone.jpg', content_type='image/jpeg')


def demoimagetwo(request):
    return render(request,'assets/img/demoimagetwo.jpg', content_type='image/jpeg')


def shopping_cart(request):
    return render(request, 'shoppingCart.html')


def bikes_view(request):
    try:
        bikeApiResponse = requests.get("http://52.23.167.11/api/shop/showbikes?perpage=100&minprice=1")
        apiData = json.loads(bikeApiResponse.content)
        return render(request, 'bikes.html', apiData)
    # IF API is offline
    except:
        return render(request, 'bikes.html')


def bikes_get(request):
    # Building the query string
    try:
        apiString = "http://52.23.167.11/api/shop/showbikes?perpage=100&"
        # First, figure out the filter criterias
        first = True
        for criteria in request.GET:
            # Then for each criteria, append the criteria to the string then get all the individual elements into a comma separated string,
            if first:
                apiString += criteria + '='
            else:
                apiString += '&' + criteria + '='
            first = False
            for value in request.GET.getlist(criteria):
                # Append values to the string
                if criteria == "minprice" or criteria == "maxprice":
                    # Removing excess comma for minprice and maxrpice due to the api not handling it well
                    if value == "":
                        value = 0
                    try:
                        # Changing value to a float, if not it ignores it
                        value = float(value)
                        # setting default values
                        if value <= 0 and criteria == "minprice":
                            value = 1
                        elif value == 0 and criteria == "maxprice":
                            value = 10000

                        apiString += str(value)
                    except:
                        apiString = apiString[:-10]
                else:
                    apiString += value + ','
        bikeApiResponse = requests.get(apiString)
        apiData = json.loads(bikeApiResponse.content)
        if apiData['status'] == "fail":
            apiData = {}
        return render(request, 'bikes.html', apiData)
    # Could not connect to API
    except:
        return render(request, "bikes.html")


def bike_details(request, bikeid):
    try:
        bikeApiResponse = requests.get("http://52.23.167.11/api/shop/showbikes?perpage=100&minprice=1&bikeid=" + str(bikeid))
        apiData = json.loads(bikeApiResponse.content)
        if apiData['status'] == "fail":
            apiData = {}
        if not apiData['data']:
            raise Http404
        return render(request, 'bike_details.html', apiData)
    # IF API is offline or bike does not exist
    except:
        raise Http404

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def logout_view(request):
    logout(request)
    return  redirect('/')

